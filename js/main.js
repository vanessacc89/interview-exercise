// $.getJSON("data.json", function(data) {
$.getJSON("https://randomuser.me/api/?results=1&noinfo", function(data){
    var person = data["results"][0];

    $(".panel-button").click(function(event){
        $(".panel-button").removeClass('active')

        var element = $(event.currentTarget)
        var attribute = element.data('attribute')
        element.addClass('active')

        console.log(attribute)
        
        var info = "";

        if(attribute == "user"){
            info = person['name']['first']+' '+person['name']['last']
        } else if(attribute == "email"){
            info = person['email']
        } else if(attribute == "dob"){
            var dob = new Date(person['dob']['date'])
            info = (dob.getMonth()+1)+"/"+dob.getDate()+"/"+dob.getFullYear()
        } else if(attribute == "phone"){
            info = person['phone']
        } else if(attribute == "location"){
            info = person['location']["street"]+", "+person['location']["city"]+", "+person['location']["state"]
        }

        $("#main-panel").html(info)
    });
});
